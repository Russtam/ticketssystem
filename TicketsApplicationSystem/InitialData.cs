﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketsApplicationSystem.Models;

namespace TicketsApplicationSystem
{
    public class InitialData
    {
        public static void InitData(TicketsDbContext context)
        {
            var dateCreation = DateTime.Now;
            context.Executors.AddRange(
                new Executor("Дежурный", "Программист"),
                new Executor("Петров Илья", "Ведущий программист"),
                new Executor("Васильев Иван", "Программист"),
                new Executor("Иванов Артем", "аналитик"),
                new Executor("Антонов Антон", "Программист")
                );
            context.States.AddRange(
                new State("Открыта"),
                new State("Решена"),
                new State("Возврат"),
                new State("Закрыта"),
                new State("Отменена")
                );
            context.Tickets.AddRange(
                new Ticket
                {
                    Id = new Guid(),
                    Title = "Исправить проверку ввода данных",
                    Description = "При вводе данных происходит необъяснимые вещи",
                    DateCreation = dateCreation,
                    DateChanged = dateCreation,
                    StateId = 1,
                    ExecutorId = 1
                    

                },
                new Ticket
                {
                    Id = new Guid(),
                    Title = "не работает соритровка по имени",
                    Description = "При нажатии на сортировку, список не отсоритровывается. Необходимо поправить логику отрабатывания кнопки",
                    DateCreation = dateCreation.AddHours(-3),
                    DateChanged = dateCreation.AddHours(-2),
                    StateId = 2,
                    ExecutorId = 1
                },
                new Ticket
                {
                    Id = new Guid(),
                    Title = "Нет подключения к базе данных",
                    Description = "Отсутствует подключение к БД. Проверить строку подключения",
                    DateCreation = dateCreation.AddDays(-2),
                    DateChanged = dateCreation.AddDays(-1),
                    StateId = 3,
                    ExecutorId = 1

                },
                new Ticket
                {
                    Id = new Guid(),
                    Title = "Не найдена страница по адресу",
                    Description = "Не найдена страница по искомому адресу. Исправить проблему",
                    DateCreation = dateCreation.AddDays(-5),
                    DateChanged = dateCreation.AddDays(-1).AddHours(2),
                    StateId = 1,
                    ExecutorId = 1
                }
            ) ;
           
            context.SaveChanges();
        }

    }
}
