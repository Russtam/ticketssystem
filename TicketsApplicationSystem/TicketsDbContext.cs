﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketsApplicationSystem.Models;

namespace TicketsApplicationSystem
{
    public class TicketsDbContext : DbContext
    {
        public TicketsDbContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }
        public TicketsDbContext() { }
        
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Executor> Executors { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<TicketHistory> Histories {get;set;}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
           //Это на будущее, когда база перерастет за 100 000. В случае в малых количествах данных индексы бесполезны и даже вредны
            modelBuilder.Entity<Ticket>().HasKey(ticket => ticket.TicketNumber);
            modelBuilder.Entity<Ticket>().HasIndex(item => new { item.StateId, item.DateChanged });
            modelBuilder.Entity<Ticket>().HasIndex(item => item.DateCreation);
            modelBuilder.Entity<TicketHistory>().HasIndex(item => item.TicketId);
            base.OnModelCreating(modelBuilder);
        }
    }
}
