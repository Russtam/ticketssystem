﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TicketsApplicationSystem.ViewModels
{
    public class TicketViewModel
    {
        [Display(Name = "Номер заявки")]
        public int TicketNumber { get; set; }
        [Display(Name="Название")]
        public string Title { get; set; }
        [Display(Name="Описание")]
        public string Description { get; set; }
        [Display(Name="Дата создания")]
        public DateTime DateCreation {get;set;}
        [Display(Name="Статус")]
        public int StateId { get; set; } = 1;
        [Display(Name="Исполнитель")]
        public int ExecutorId { get; set; } = 1;
        [Display(Name="Комментарий")]
        public string Comment { get; set; }
        public DateTime? DateChanged { get; set; }
    }
}
