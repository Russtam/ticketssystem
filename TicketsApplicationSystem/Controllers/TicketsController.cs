﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TicketsApplicationSystem.ModelBuilders;
using TicketsApplicationSystem.Models;
using TicketsApplicationSystem.Repository;
using TicketsApplicationSystem.ViewModels;

namespace TicketsApplicationSystem.Controllers
{
    public class TicketsController : Controller
    {
        private readonly ITicketsRepository _ticketsRepository;
        private readonly ITicketsModelBuilder _modelBuilder;

        public TicketsController(TicketsDbContext context, ITicketsRepository ticketsRepository, ITicketsModelBuilder modelBuilder)
        {
            _ticketsRepository = ticketsRepository;
            _modelBuilder = modelBuilder;
        }

        public async Task<IActionResult> Index()
        {
            var ticketsDbContext = _ticketsRepository.GetAllTickets();
            return View(await ticketsDbContext.ToListAsync());
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ticket = await _ticketsRepository.GetTicketDeailsAsync(id);
            if (ticket == null)
            {
                return NotFound();
            }
            return View(ticket);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("TicketNumber,Title,Description,DateCreation,DateChanged,StateId,ExecutorId")] Ticket ticket)
        {
            if (ModelState.IsValid)
            {
                await Task.Run(() => _ticketsRepository.CreateTicket(ticket));
                return RedirectToAction(nameof(Index));
            }
            return View(ticket);
        }



        public async Task<IActionResult> Edit(int id)
        {
            var ticket = await Task.Run(() => _modelBuilder.GetTicketById(id));
            if (ticket == null)
            {
                return NotFound();
            }
            if (_ticketsRepository.States != null)
                ViewData["StateName"] = _ticketsRepository.States;
            if (_ticketsRepository.Executors != null)
                ViewData["ExecutorName"] = _ticketsRepository.Executors;
            return View(ticket);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, TicketViewModel ticket)
        {
            if (id != ticket.TicketNumber)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await Task.Run(()=>_ticketsRepository.Update(ticket, id));
                }
                catch (DbUpdateConcurrencyException)
                {
                    return NotFound();
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ExecutorName"] = _ticketsRepository.Executors;
            return View(ticket);
        }

        public async Task<IActionResult> Delete(int id)
        {
            var ticket = await Task.Run(() => _ticketsRepository.RemoveTicket(id));
            if (ticket == null)
            {
                return NotFound();
            }

            return View(ticket);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await Task.Run(() => _ticketsRepository.RemoveTicket(id));
            return RedirectToAction(nameof(Index));
        }
    }
}
