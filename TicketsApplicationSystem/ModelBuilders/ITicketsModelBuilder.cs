﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketsApplicationSystem.ViewModels;

namespace TicketsApplicationSystem.ModelBuilders
{
    public interface ITicketsModelBuilder
    {
        IQueryable<TicketViewModel> GetTickets();
        TicketViewModel GetTicketById(int id);
    }
}
