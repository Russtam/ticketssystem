﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketsApplicationSystem.Models;
using TicketsApplicationSystem.Repository;
using TicketsApplicationSystem.ViewModels;

namespace TicketsApplicationSystem.ModelBuilders
{
    public class TicketBuilder : ITicketsModelBuilder
    {
        private readonly ITicketsRepository _ticketRepository;
        public TicketBuilder (ITicketsRepository ticketRepository)
        {
            _ticketRepository = ticketRepository;
        }
        
        public TicketViewModel GetTicketById(int id)
        {
            var ticket = _ticketRepository.EditTicket(id);
            return new TicketViewModel
            {
                TicketNumber = ticket.TicketNumber,
                Title = ticket.Title,
                DateChanged = ticket.DateChanged,
                DateCreation = ticket.DateCreation,
                Description = ticket.Description,
                ExecutorId = ticket.ExecutorId,
                StateId = ticket.StateId
            };
        }

        public IQueryable<TicketViewModel> GetTickets()
        {
            var ticketList = new List<TicketViewModel>();
            var tickets = _ticketRepository.GetAllTickets();
            foreach(var ticket in tickets)
            {
                ticketList.Add(new TicketViewModel
                {
                    DateChanged = ticket.DateChanged,
                    DateCreation = ticket.DateCreation,
                    Description = ticket.Description,
                    StateId = ticket.StateId,
                    ExecutorId = ticket.ExecutorId,
                    Title = ticket.Title,
                    TicketNumber = ticket.TicketNumber
                });
            }
            return ticketList.AsQueryable();
        }
    }
}
