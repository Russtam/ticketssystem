﻿

using System.Collections.Generic;

namespace TicketsApplicationSystem.Models
{
    public class State
    {
        public State(string stateName)
        {
            StateName = stateName;
        }
        public State() { }

        public int Id { get; set; }
        public string StateName { get; set; }
        public ICollection<Ticket> Tickets { get; set; }
        
    }
}
