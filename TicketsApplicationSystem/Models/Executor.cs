﻿using System.Collections.Generic;

namespace TicketsApplicationSystem.Models
{
    public class Executor
    {
        public Executor(string name, string position)
        {
            Name = name;
            Position = position;
        }
        public Executor() { }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
        ICollection<Ticket> Tickets { get; set; }

    }
}