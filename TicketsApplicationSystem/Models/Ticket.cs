﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TicketsApplicationSystem.Models
{
    public class Ticket
    {
        [Key]
        public Guid Id { get; set; }
        
        public int TicketNumber { get; set; }
        [MaxLength(200)]
        public string Title { get; set; }
        [MaxLength(1000)]
        public string Description { get; set; }

        public DateTime DateCreation {get;set;}
        public DateTime? DateChanged { get; set; }
       
        public int StateId { get; set; }
        public State State { get; set; }
        
        public int ExecutorId { get; set; }
        public Executor Executor { get; set; }
        public ICollection<TicketHistory> History { get; set; }

        public Ticket(Guid guid, string title, string description, DateTime dateCreation, int stateId, int execId)
        {
            Id = guid;
            Title = title;
            Description = description;
            DateCreation = dateCreation;
            DateChanged = dateCreation;
            StateId = stateId;
            ExecutorId = execId;
        }
        public Ticket() { }
    }
}
