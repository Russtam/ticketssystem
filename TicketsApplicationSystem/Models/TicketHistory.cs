﻿using System;

namespace TicketsApplicationSystem.Models
{
    public class TicketHistory
    {
        public int Id { get; set; }
        public int TicketId { get; set; }
        public string State { get; set; }
        public string Comment { get; set; }
        public DateTime? DateChanged { get; set; }

    }
}
