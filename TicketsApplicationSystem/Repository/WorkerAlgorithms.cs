﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketsApplicationSystem.Models;

namespace TicketsApplicationSystem.Repository
{
    public class WorkerAlgorithms : IWorkerAlgorithms
    {
        public IEnumerable<Ticket> FilterByDate(IEnumerable<Ticket> tickets, DateTime beginDate, DateTime endDate)
        {
            var filteredTickets = tickets
                .Where(ticket => ticket.DateCreation > beginDate && ticket.DateCreation < endDate);
            return filteredTickets;
        }

        public IEnumerable<Ticket> FilterByState(IEnumerable<Ticket> tickets, int stateId)
        {
            var filteredTickets = tickets
                .Where(ticket => ticket.StateId == stateId);
            return filteredTickets;
        }

        public IEnumerable<Ticket> SortByDate(IEnumerable<Ticket> tickets)
        {
            var ticketsArr = tickets as Ticket[];
            for (int i = 0; i < ticketsArr.Length; i++)
            {
                for (int j = 0; j < ticketsArr.Length - 1 - i; j++)
                {
                    if (ticketsArr[j].DateCreation.CompareTo(ticketsArr[j + 1].DateCreation) == 1)
                    {
                        Swap(j, j + 1, ref ticketsArr);
                    }
                }
            }
            return tickets;
        }

        public IEnumerable<Ticket> SortByState(IEnumerable<Ticket> tickets)
        {
            var ticketsArr = tickets as Ticket[];
            for (int i = 0; i < ticketsArr.Length; i++)
            {
                for (int j = 0; j < ticketsArr.Length - 1 - i; j++)
                {
                    if (ticketsArr[j].State.StateName.CompareTo(ticketsArr[j + 1].State.StateName) == 1)
                    {
                        Swap(j, j + 1, ref ticketsArr);
                    }
                }
            }
            return tickets;
        }
        private void Swap(int i, int j, ref Ticket[] records)
        {
            if (i < records.Length && j < records.Length)
            {
                var temp = records[i];
                records[i] = records[j];
                records[j] = temp;
            }
        }
    }

}
