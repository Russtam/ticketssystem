﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketsApplicationSystem.Models;

namespace TicketsApplicationSystem.Repository
{
    public interface IWorkerAlgorithms
    {
        IEnumerable<Ticket> SortByState(IEnumerable<Ticket> tickets);
        IEnumerable<Ticket> SortByDate(IEnumerable<Ticket> tickets);
        IEnumerable<Ticket> FilterByState(IEnumerable<Ticket> tickets, int stateId);
        IEnumerable<Ticket> FilterByDate(IEnumerable<Ticket> tickets, DateTime beginDate, DateTime endDate);
    }
}
