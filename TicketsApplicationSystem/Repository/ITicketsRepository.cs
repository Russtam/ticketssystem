﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketsApplicationSystem.Models;
using TicketsApplicationSystem.ViewModels;

namespace TicketsApplicationSystem.Repository
{
    public interface ITicketsRepository
    {
        SelectList States { get;}
        SelectList Executors { get;}
        IQueryable<Ticket> GetAllTickets();
        IEnumerable<Ticket> GetTicketsByStatus(IEnumerable<Ticket> tickets, int state);
        IEnumerable<Ticket> GetTicketsForPeriod(IEnumerable<Ticket> tickets, DateTime beginTime, DateTime endTime);
        IQueryable<Ticket> GetSortedTickets(IEnumerable<Ticket> tickets, bool isSortByState);
        Ticket EditTicket(int ticketNumber);
        Ticket RemoveTicket(int id);
        void CreateTicket(Ticket ticket);
        Task<Ticket> GetTicketDeailsAsync(int? ticketNumber);
        void Save();
        void Update(TicketViewModel ticket, int id);
    }
}
