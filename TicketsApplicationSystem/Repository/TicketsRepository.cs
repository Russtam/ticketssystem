﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketsApplicationSystem.ModelBuilders;
using TicketsApplicationSystem.Models;
using TicketsApplicationSystem.ViewModels;

namespace TicketsApplicationSystem.Repository
{
    public class TicketsRepository : ITicketsRepository, IEnumerable
    {
        public SelectList States { get; }
        public SelectList Executors { get; }
        TicketsDbContext _context;
        private readonly IWorkerAlgorithms _algorithms;
        public TicketsRepository(TicketsDbContext context, IWorkerAlgorithms algorithms)
        {
            States = new SelectList(context.States, "Id", "StateName");
            Executors = new SelectList(context.Executors, "Id", "Name");
            _context = context;
            _algorithms = algorithms;
        }
        public void CreateTicket(Ticket ticket)
        {
            _context.Tickets.Add(ticket);
            _context.SaveChanges();
        }
        public IQueryable<Ticket> GetAllTickets() =>
            _context.Tickets
                .Include(t => t.Executor)
                .Include(t => t.State);
        public IQueryable<Ticket> GetSortedTickets(IEnumerable<Ticket> tickets, bool isSortByState)
        {
            if (tickets != null)
            {
                if (isSortByState)
                    return _algorithms.SortByState(tickets).AsQueryable();
                return _algorithms.SortByDate(tickets).AsQueryable();
            }
            else
                throw new ArgumentNullException();
        }

        public Task<Ticket> GetTicketDeailsAsync(int? ticketNumber) =>  _context.Tickets
                .Include(t => t.Executor)
                .Include(t => t.State)
                .FirstOrDefaultAsync(m => m.TicketNumber == ticketNumber);

        public IEnumerable<Ticket> GetTicketsByStatus(IEnumerable<Ticket> tickets, int state) => _algorithms.FilterByState(tickets, state);
        

        public IEnumerable<Ticket> GetTicketsForPeriod(IEnumerable<Ticket> tickets, DateTime beginDate, DateTime endDate) => _algorithms.FilterByDate(tickets, beginDate, endDate);
        

        public Ticket RemoveTicket(int id)
        {
            var ticket = _context.Tickets
                .Include(t => t.Executor)
                .FirstOrDefault(m => m.TicketNumber == id);
            if (ticket == null)
            {
                throw new ArgumentNullException();
            }
            _context.Entry(ticket).State = EntityState.Deleted;
            _context.SaveChanges();
            return ticket;
        }
        public void Save()
        {
            throw new NotImplementedException();
        }

        public void Update(TicketViewModel ticket, int ticketNumber)
        {
            var currTicker = _context.Tickets.FirstOrDefault(t => t.TicketNumber == ticketNumber);
            if (currTicker!=null)
            {
                currTicker.State = _context.States.Find(ticket.StateId);
                currTicker.DateChanged = DateTime.Now;
                currTicker.Description = ticket.Description;
                currTicker.Executor = _context.Executors.Find(ticket.ExecutorId);
                currTicker.Title = ticket.Title;
                if (currTicker.History == null)
                    currTicker.History = new List<TicketHistory>();
                HistoryWrite(currTicker, ticket);
            }
            _context.SaveChanges();
        }

        private void HistoryWrite(Ticket currTicker, TicketViewModel ticket)
        {
            if (ticket.StateId == 1 && currTicker.StateId == 2)
            {
                currTicker.History.Add(new TicketHistory
                {
                    DateChanged = currTicker.DateChanged,
                    TicketId = currTicker.TicketNumber,
                    State = currTicker.State.StateName,
                    Comment = ticket.Comment
                }) ;
            }
            currTicker.History.Add(new TicketHistory
            {
                DateChanged = currTicker.DateChanged,
                TicketId = currTicker.TicketNumber,
                State = currTicker.State.StateName,
                Comment = ticket.Comment
            });
        }

        public Ticket EditTicket(int ticketNumber)
        {

            var ticket = _context.Tickets
                .Include(t => t.State)
                .Include(t => t.Executor)
                .FirstOrDefault(t => t.TicketNumber == ticketNumber);
            return ticket;
        }

        public IEnumerator GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
